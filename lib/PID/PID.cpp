#include "PID.h" 
#include <LibRobus.h>
 int nbPulseParCycle0 = 0;    //Valeur statique
 int nbPulseParCycle1 = 0;    //Valeur statique
 double vitesseMoteur = 0;   //Valeur statique
 

void PID(double valMoteurInitiale)
{
    
  
    float erreurP = 0;
    float erreurI = 0;
    float correction = 0;
    //double Kp = 0.00005;    //robot 2
    //double Ki = 0.00003;

    double Kp = KpRobot1;    //robot 1
    double Ki = KiRobot1;
    

    nbPulseParCycle0 = nbPulseParCycle0 + ENCODER_Read(0);
    nbPulseParCycle1 = nbPulseParCycle1 + ENCODER_Read(1);
    

    erreurP =  0.003 +(double)(nbPulseParCycle0 - nbPulseParCycle1) * Kp;
    erreurI = (ENCODER_Read(0) - ENCODER_Read(1)) * Ki;
    correction = erreurP + erreurI;
    
    vitesseMoteur = valMoteurInitiale + correction;
    //  Serial.print("vitessemoteur");
    //  Serial.println(vitesseMoteur,5);
    
    //   Serial.print("erreurP");
    //   Serial.println(erreurP,5);
    
    MOTOR_SetSpeed(1, vitesseMoteur);
    MOTOR_SetSpeed(0, valMoteurInitiale);
    

    ENCODER_ReadReset(0);
    ENCODER_ReadReset(1);
    delay(30);
}

void PIDR(double valMoteurInitiale)
{
    
  
    float erreurP = 0;
    float erreurI = 0;
    float correction = 0;
    double Kp = 0.00005;    //robot 2
    double Ki = 0.00003;
    

    nbPulseParCycle0 = nbPulseParCycle0 + ENCODER_Read(0);
    nbPulseParCycle1 = nbPulseParCycle1 + ENCODER_Read(1);
    

    erreurP =  0.001 + (double)(nbPulseParCycle0 - nbPulseParCycle1) * Kp;
    erreurI = (ENCODER_Read(0) - ENCODER_Read(1)) * Ki;
    correction = erreurP + erreurI;
    
    vitesseMoteur = valMoteurInitiale + correction;
    //  Serial.print("vitessemoteur");
    //  Serial.println(vitesseMoteur,5);
    
    //   Serial.print("erreurP");
    //   Serial.println(erreurP,5);
    
    MOTOR_SetSpeed(1, vitesseMoteur);
    MOTOR_SetSpeed(0, valMoteurInitiale);
    

    ENCODER_ReadReset(0);
    ENCODER_ReadReset(1);
    delay(30);
}


void PID_recule(double valMoteurInitiale)
{
    
  
    float erreurP = 0;
    float erreurI = 0;
    float correction = 0;
    //double Kp = 0.00005;    //robot 2
    //double Ki = 0.00003;

    double Kp = KpRobot1;    //robot 1
    double Ki = KiRobot1;
    

    nbPulseParCycle0 = nbPulseParCycle0 + ENCODER_Read(0);
    nbPulseParCycle1 = nbPulseParCycle1 + ENCODER_Read(1);
    

    erreurP =  0.003 +(double)(nbPulseParCycle0 - nbPulseParCycle1) * Kp;
    erreurI = (ENCODER_Read(0) - ENCODER_Read(1)) * Ki;
    correction = erreurP + erreurI;
    
    vitesseMoteur = valMoteurInitiale + correction;
     Serial.print("vitessemoteur");
     Serial.println(vitesseMoteur,5);
    
      Serial.print("erreurP");
      Serial.println(correction,5);
    
    MOTOR_SetSpeed(1, vitesseMoteur);
    MOTOR_SetSpeed(0, valMoteurInitiale);
    

    ENCODER_ReadReset(0);
    ENCODER_ReadReset(1);
    delay(3000);
}