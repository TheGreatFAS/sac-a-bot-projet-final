/**
 * \file   suiveur_ligne.h
 * \author Team Sac-A-Bot
 * \date   22 octobre 2019
 * \brief  Entête du fichier suiveur_ligne.cpp
 * \version 1.0
 */

#ifndef SUIVEUR_LIGNE_H
#define SUIVEUR_LIGNE_H

#include <LibRobus.h> // Essentielle pour utiliser RobUS

void suivre_ligne();
void suivre_ligne_droite();
void oriente_gauche();
void oriente_droite();
int est_perpendiculaire();
int est_parallele();
int est_cote_gauche();
int est_cote_droit();
int capteurs_actives();
int est_un_coin_vers_droite();
int est_un_coin_vers_gauche();
int capteurs_actives_numerique();

#endif