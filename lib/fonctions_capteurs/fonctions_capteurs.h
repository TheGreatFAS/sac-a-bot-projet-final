/**
 * \file   FonctionsCapteurs.hpp
 * \author Team Sac-A-Bot
 * \date   22 octobre 2019
 * \brief  Entète du fichier fonctions_capteurs.cpp
 * \version 1.0
 */

#ifndef FONCTIONSCAPTEURS_H
#define FONCTIONSCAPTEURS_H

#include <LibRobus.h>
#include <math.h>
#include <Arduino.h>
#include <string.h>

#define TAG_RFID_RFTR005 "0415149500" //déneige
#define TAG_RFID_RFTR012 "0415149b15" //déneige
#define TAG_RFID_RFTR013 "0414e50dcf" //déneige pas

#define SUIVEUR_BROCHE_GAUCHE 40
#define SUIVEUR_BROCHE_CENTRE 39
#define SUIVEUR_BROCHE_DROITE 38



struct Couleur
{
    unsigned char rouge;
    unsigned char vert;
    unsigned char bleu;
};

struct Lecture_Suiveur_Numerique
{
    bool gauche;
    bool centre;
    bool droite;
};

double lire_capteur_distance(int id);
double lire_median_capteur_distance(int id);
double lire_suiveur_ligne();
String lire_rfid();
void rfid_demarre_serie();
unsigned char rfid_ascii_to_hex(unsigned char val_ascii);
void rfid_clear_buffer();
Couleur lire_capteur_couleur();
Lecture_Suiveur_Numerique lire_suiveur_ligne_numerique();

#endif