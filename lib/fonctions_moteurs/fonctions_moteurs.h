/**
 * \file   fonctions_moteurs.h
 * \author Team Sac-A-Bot
 * \date   22 octobre 2019
 * \brief  Entête du fichier fonctions_moteurs.cpp
 * \version 1.0
 */

#ifndef FONCTIONS_MOTEURS_H
#define FONCTIONS_MOTEURS_H

#include <LibRobus.h> // Essentielle pour utiliser RobUS
#include <math.h>     //Importe les fonctions de mathématique


#define DISTANCE_ROUES_28  18.5 //Distance entre les 2 roues en cm
#define DISTANCE_ROUES_02 18.5 //Distance entre les 2 roues en cm
#define DEMI_FORCE 0.5 //Défini la force de la moitié des moteurs
#define VITESSE_VIRAGE 0.3 //Défini la vitesse lors des virages
#define VITESSE_LIGNE_DROITE 0.4 //Défini la vitesse lors des lignes droites

#define MOTEUR_GAUCHE 0 //ID du moteur gauche
#define MOTEUR_DROITE 1 //ID du moteur droit

#define PELLE_GAUCHE 0  //Sens de rotation de la pelle
#define PELLE_DROITE 1

/**
 * \brief Fonction servant à remettre les compteurs de clics d'encodeurs à 0.
 * \param Aucun
 * \return Aucun
 */
void reset_encoders();

/**
 * \brief Fonction servant à faire tourner le robot à gauche.
 * \param degre Angle de rotation en degrés.
 * \return Aucun
 */
void tourner_gauche(int degre);

/**
 * \brief Fonction servant à faire tourner le robot à gauche.
 * \param degre Angle de rotation en degrés.
 * \return Aucun
 */
void tourner_droite(int degre);

/**
 * \brief Fonction servant à faire avancer le robot en ligne droite.
 * \param longueur Distance en cm.
 * \return Aucun
 */
void ligne_droite(int longueur);

/**
 * \brief Fonction servant à faire un demi tour avec le robot.
 * \param Aucun
 * \return Aucun
 */
void demi_tour();

/**
 * \brief Fonction servant à garder les roues du robot à la même vitesse.
 * \param valMoteurInitiale Vitesse initiale du moteur.
 * \return Aucun
 */
void PID(double valMoteurInitiale);

void PIDR(double valMoteurInitiale);

/** 
 * \brief Fonction servant à accélérer le robot lorsqu'il avance en ligne droite
 * \param valMoteurInitiale Vitesse du moteur où l'ajustement est fait
 * \return Aucun
 */
void acceleration(int pulse_parcourue);

/** 
 * \brief Fonction servant à décelérer le robot lorsqu'il avance en ligne droite
 * \param Aucun Sert a calculer le nombre de pulse parcouru dans la fonction d'acceleration aussi
 * \return Aucun
 */
void deceleration(int vitesse);

/**
 * \brief Fait avancer le robot en ligne droite sans fin
 * 
 * Fait avancer le robot en ligne droite sans que celui-ci ne s'arrête
 * 
 * \param 'vitesse' représente la vitesse initiale des moteurs AVANT la décélération voulue.
 */
void avancer(float vitesse);

/**
 * \brief Centre la pelle du robot.
 */
void centrer_pelle();

/**
 * \brief Change l'orientation de la pelle.
 * 
 * \param sens Le sens de la pelle.
 */
void deplacer_pelle(unsigned char sens);

/**
 * \brief Lève la pelle.
 */
void lever_pelle();

/**
 * \brief Baisse la pelle.
 */
void baisser_pelle();

/**
 * \brief Tourne à droite sur lui-même
 */
void tourner_droite_luimeme(int degre);

/**
 * \brief Tourne à gauche sur lui-même
 */
void tourner_gauche_luimeme(int degre);

void stop ();

void reculer(float vitesse);

void reculer_ligne_droite(int longueur);
#endif