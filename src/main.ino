/**
  *Projet: Sac-à-Bot
  *Equipe: P-01
  *Auteurs: Les membres auteurs du script
  *Description: Breve description du script
  *Date: Derniere date de modification
*/

/* ****************************************************************************
Inclure les librairies de functions que vous voulez utiliser
**************************************************************************** */

#include <LibRobus.h>           // Essentielle pour utiliser RobUS
#include <math.h>               //Importe les fonctions de mathématique
#include <PID.h>                //Importe les fonctions du programme PID.c
#include <fonctions_moteurs.h>  //Importe les fonctions des moteurs (tourner, avancer ligne droite, accel...)
#include <fonctions_capteurs.h> //Importe les fonctions qui gérent les différents capteurs
#include <suiveur_ligne.h>      //Importe les fonctions qui permettent de suivre des lignes
#include "parcour_deneigement.h"    //Importe les fonctions qui permettent de passer de cour en cours


/* ****************************************************************************
Variables globales et defines
**************************************************************************** */
// -> defines...
// L'ensemble des fonctions y ont acces

/* ****************************************************************************
Vos propres fonctions sont creees ici
**************************************************************************** */
void sequence_audit()
{
  bool pelle_levee = false;
  while (1)
  {
    String temp = lire_rfid();

    if (temp != "")
    {
      if (temp == TAG_RFID_RFTR005)
      {
        deplacer_pelle(PELLE_GAUCHE);
      }

      else if (temp == TAG_RFID_RFTR012)
      {
        deplacer_pelle(PELLE_DROITE);
      }

      else if (temp == TAG_RFID_RFTR013)
      {
        if (!pelle_levee)
        {
          lever_pelle();
          pelle_levee = true;
        }

        else
        {
          baisser_pelle();
          pelle_levee = false;
        }
      }
    }

    delay(1000);
  }
}

/**
 * \brief Méthode principale appelée par la loop pour le déneigement.
 * 
 * -----Méthode bloquante-----
 * 
 * La méthode est basée sur la méthode suivre_ligne() afin d'implémenter le
 * suiveur de ligne dans la méthode en tant que tel.
 */
void sequence_deneigement()
{
  int compteur_perpendiculaire = 0;
  String temp = "";

  while (1)
  {
    if (est_perpendiculaire() == 1)
    {
      compteur_perpendiculaire++;
      

      if(compteur_perpendiculaire == 3 || compteur_perpendiculaire == 6)
      {
        rfid_clear_buffer();
        ligne_droite(1);
        tourner_gauche_luimeme(90);
      }

      if(compteur_perpendiculaire == 1 || compteur_perpendiculaire == 4 || compteur_perpendiculaire == 8)
      {
        int i=0;
        temp = lire_rfid();
        stop();

        while(temp == "")
        {

          delay(300);
          ligne_droite(1);
          i++;
          temp = lire_rfid();
        }
        reculer_ligne_droite(i);
          if (temp == TAG_RFID_RFTR005 || temp == TAG_RFID_RFTR012 )
          {
            double distance;
            reculer_ligne_droite(15);
            tourner_droite_luimeme(90);
            ligne_droite(19);
            deplacer_pelle(PELLE_DROITE);
            baisser_pelle();
            reset_encoders();
            while(( distance = lire_median_capteur_distance(0)) >= 33 || distance == -1 )
            {
              
              Serial.print("distance :");
              Serial.println(distance);
              reculer(0.15);
            }
            stop();
            lever_pelle();
            delay(1000);
            deplacer_pelle(PELLE_GAUCHE);
            delay(1000);
            deplacer_pelle(PELLE_DROITE);
            while(est_perpendiculaire() == 1)
            {
              avancer(0.15);
            }
            delay(1000);
            while(est_perpendiculaire() == 0)
            {
              avancer(0.15);
            }
            stop();
            
            tourner_gauche_luimeme(90);
            reculer_ligne_droite(15);
            tourner_droite_luimeme(90);
            
            ligne_droite(19);
            deplacer_pelle(PELLE_GAUCHE);
            baisser_pelle();
            reset_encoders();
            while(( distance = lire_median_capteur_distance(0)) >= 33 || distance == -1 )
            {
              reculer(0.15);
            }
            stop();
            lever_pelle();
            deplacer_pelle(PELLE_DROITE);
            delay(1000);
            deplacer_pelle(PELLE_GAUCHE);
            delay(1000);
            while(capteurs_actives_numerique() != 0)
            {
              avancer(0.15);
            }
            while(capteurs_actives_numerique() != 123)
            {
              avancer(0.15);
            }
            stop();
            
            ligne_droite(1);
            tourner_gauche_luimeme(90);

          }

          else if (temp == TAG_RFID_RFTR013)
          {
            compteur_perpendiculaire++;
            ligne_droite(4);
          }
        

        
          }

      if(compteur_perpendiculaire == 2 || compteur_perpendiculaire == 5 || compteur_perpendiculaire == 7)
      {
        ligne_droite(4);
      }
      if(compteur_perpendiculaire == 9)
      {
        stop();
        while(1);
      }

      reset_encoders();
      delay(20);
      if (ENCODER_Read(1) == 0)
      {
        acceleration(0);
      }
      avancer(0.15);
    }
    else if (est_parallele() == 1)
    {   
       
        reset_encoders();
        delay(20);
        if(ENCODER_Read(1) == 0)
        {
            acceleration(0);
        }
       
        avancer(0.15);

    }
    
    else if (est_cote_droit())
    {
      
      oriente_droite();
    }
    else if (est_cote_gauche())
    {
      
      oriente_gauche();
    }
    else if (capteurs_actives_numerique() == 0)
    {
      avancer(0.15);
    }

    delay(1); //Délai pour décharger le CPU
  }
}

void deneiger()
{
  
}

/* ****************************************************************************
Fonctions d'initialisation (setup)
**************************************************************************** */
// -> Se fait appeler au debut du programme
// -> Se fait appeler seulement une fois
// -> Generalement on y initialise les variables globales

void setup()
{
  SERVO_Enable(0);
  SERVO_Enable(1);
  baisser_pelle();
  BoardInit();
}

/* ****************************************************************************
Fonctions de boucle infini (loop())
**************************************************************************** */
// -> Se fait appeler perpetuellement suite au "setup"

void loop()
{
  // reset_encoders();
  // while(1) reculer(0.15);
   lever_pelle();
  sequence_deneigement();
  //capteurs_actives_numerique();
  // suivre_ligne();
  // prochaine_cour();
  // MOTOR_SetSpeed(0,-0.3);
  // MOTOR_SetSpeed(1,-0.3);
  // delay(10000);
  // ligne_droite(10);
  // delay(5000);
  delay(10000);
}