/**
 * \file   suiveur_ligne.cpp
 * \author Team Sac-A-Bot
 * \date   22 octobre 2019
 * \brief  Librairie des fonctions permettant au robot de suivre une ligne
 * \version 1.0
 */

#include "suiveur_ligne.h" /**Importe la librairie des fonctions qui permettent
                                de suivre des lignes */
#include "PID.h"
#include "fonctions_capteurs.h"
#include <LibRobus.h>
#include "fonctions_moteurs.h"

#define ERREUR 0.1
#define CAPTEUR_1 3.07 //2.87109375
#define CAPTEUR_2 1.53 //1.440429688
#define CAPTEUR_3 0.76 //0.666

/**
*\brief Permet au robot de suivre une ligne.
*
*Ajuste les moteurs pour permettre au robot de suivre la ligne selon
*   les valeurs des capteurs du suiveur de ligne. Tant que le robot
*   ne croise pas de ligne, la fonction ne fait rien.
*
*/
void suivre_ligne()
{
    if (est_parallele() == 1)
    {
        //Serial.println("PARRALLELE");
        reset_encoders();
        delay(20);
        if (ENCODER_Read(1) == 0)
        {
            acceleration(0);
        }

        avancer(0.15);
    }
    else if (est_perpendiculaire() == 1)
    {
        Serial.println("PERPENDICULAIRE");
        reset_encoders();
        ligne_droite(1);
        tourner_gauche_luimeme(90);
    }
    else if (est_cote_droit())
    {
        Serial.println("COTE_DROIT");
        oriente_droite();
    }
    else if (est_cote_gauche())
    {
        Serial.println("COTE_GAUCHE");
        oriente_gauche();
    }
    else if (capteurs_actives_numerique() == 0)
    {
        avancer(0.15);
    }
}

/**
*\brief Permet au robot de suivre une ligne droite.
*
*Ajuste les moteurs pour permettre au robot de suivre la ligne selon
*   les valeurs des capteurs du suiveur de ligne. 
*
*/
void suivre_ligne_droite()
{
    if (est_parallele() == 1)
    {
        //Serial.println("PARRALLELE");
        reset_encoders();
        delay(20);
        if (ENCODER_Read(1) == 0)
        {
            acceleration(0);
        }
        avancer(0.15);
    }
    else if (est_cote_droit())
    {
        Serial.println("COTE_DROIT");
        oriente_droite();
    }
    else if (est_cote_gauche())
    {
        Serial.println("COTE_GAUCHE");
        oriente_gauche();
    }
    else if (capteurs_actives_numerique() == 0)
    {
        avancer(0.15);
    }
}

/**
*\brief Permet au robot de dévier légèrement vers la gauche
*
*Ajuste les moteurs pour permettre au robot de dévier lentement vers la gauche
*
*
*/
void oriente_gauche()
{
    MOTOR_SetSpeed(0, 0.15);
    MOTOR_SetSpeed(1, 0.23);
}

/**
*\brief Permet au robot de dévier légèrement vers la droite
*
*Ajuste les moteurs pour permettre au robot de dévier lentement vers la droite
*
*
*/
void oriente_droite()
{
    MOTOR_SetSpeed(0, 0.23);
    MOTOR_SetSpeed(1, 0.15);
}

/**
*\brief Détecte si le robot est perpendiculaire à une ligne.
*
*Si les 3 capteurs sont activés, alors le robot est perpendiculaire
*   à une ligne.
*
*\return Retourne 1 si le robot est perpendiculaire à une ligne, retourne 0
*   si le robot n'est pas perpendiculaire à une ligne.
*/
int est_perpendiculaire()
{
    if (capteurs_actives_numerique() == 123)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    return 0;
}

/**
*\brief Détecte si le robot arrive à un virage en coin.
*
*\return Retourne 1 si le robot est à un coin, retourne 0
*   si le robot n'est pas à un coin.
*/
int est_un_coin_vers_droite()
{
    if (capteurs_actives_numerique() == 12)
    {
        return 1;
    }
    return 0;
}

/**
*\brief Détecte si le robot arrive à un virage en coin.
*
*\return Retourne 1 si le robot est à un coin, retourne 0
*   si le robot n'est pas à un coin.
*/
int est_un_coin_vers_gauche()
{
    if (capteurs_actives_numerique() == 23)
    {
        return 1;
    }
    return 0;
}

/**
*\brief Détecte si le robot est parallèle à une ligne.
*
*Si les 1 et 3 sont activés, alors le robot est parallèle
*   à une ligne.
*
*\return Retourne 1 si le robot est parallèle à une ligne, retourne 0
*   si le robot n'est pas parallèle à une ligne.
*/
int est_parallele()
{
    if (capteurs_actives_numerique() == 2)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    return 0;
}

/**
*\brief Détecte si le robot tend à avoir la ligne sur son côté gauche.
*
*\return Retourne 1 si le robot a la ligne à sa gauche, retourne 0
*   si le robot n'a pas la ligne à sa gauche.
*/
int est_cote_gauche()
{
    if (capteurs_actives_numerique() == 3 || capteurs_actives_numerique() == 23)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    return 0;
}

/**
*\brief Détecte si le robot tend à avoir la ligne sur son côté droit.
*
*\return Retourne 1 si le robot a la ligne à sa droite, retourne 0
*   si le robot n'a pas la ligne à sa droite.
*/
int est_cote_droit()
{
    if (capteurs_actives_numerique() == 1 || capteurs_actives_numerique() == 12)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    return 0;
}

/**
*\brief Détecte quel(s) capteur(s) sont/est activé(s).
*
*
*\return Retourne un entier qui détermine quel capteur est activé.
*   1: Capteur 1 (droite) activé
*   2: Capteur 2 (centre) activé
*   3: Capteur 3 (gauche) activé
*   12: Capteurs 1 et 2 activés
*   13: Capteurs 1 et 3 activés
*   23: Capteurs 2 et 3 activés
*   123: Capteurs 1, 2 et 3 activés
*   0: Aucun capteur activé
*/
int capteurs_actives()
{
    double val_suiveur = lire_suiveur_ligne();

    if ((CAPTEUR_1 - ERREUR) < val_suiveur && val_suiveur < (CAPTEUR_1 + ERREUR))
    {
        Serial.println("Capteur 1 activé");
        return 1;
    }
    else if ((CAPTEUR_2 - ERREUR) < val_suiveur && val_suiveur < (CAPTEUR_2 + ERREUR))
    {
        Serial.println("Capteur 2 activé");
        return 2;
    }
    else if ((CAPTEUR_3 - ERREUR) < val_suiveur && val_suiveur < (CAPTEUR_3 + ERREUR))
    {
        Serial.println("Capteur 3 activé");
        return 3;
    }
    else if ((CAPTEUR_1 + CAPTEUR_2 - ERREUR) < val_suiveur && val_suiveur < (CAPTEUR_1 + CAPTEUR_2 + ERREUR))
    {
        Serial.println("Capteur 1 et 2 activé");
        return 12;
    }
    else if ((CAPTEUR_1 + CAPTEUR_3 - ERREUR) < val_suiveur && val_suiveur < (CAPTEUR_1 + CAPTEUR_3 + ERREUR))
    {
        Serial.println("Capteur 1 et 3 activé");
        return 13;
    }
    else if ((CAPTEUR_2 + CAPTEUR_3 - ERREUR) < val_suiveur && val_suiveur < (CAPTEUR_2 + CAPTEUR_3 + ERREUR))
    {
        Serial.println("Capteur 2 et 3 activé");
        return 23;
    }
    else if ((CAPTEUR_1 + CAPTEUR_2 + CAPTEUR_3 - 0.4) < val_suiveur && val_suiveur < (CAPTEUR_1 + CAPTEUR_2 +CAPTEUR_3 + 0.2))
    {
        Serial.println("3 capteurs activés");
        return 123;
    }
    else
    {
        return 0;
    }
    return 0;
}

int capteurs_actives_numerique()
{
    Lecture_Suiveur_Numerique lecture =  lire_suiveur_ligne_numerique();

    if (lecture.centre && lecture.gauche && lecture.droite)
    {
        
        return 123;
    }
    else if (lecture.droite && lecture.centre)
    {
       
        return 12;
    }
    else if (lecture.droite && lecture.gauche)
    {
        
        return 13;
    }
    else if (lecture.centre && lecture.gauche)
    {
        
        return 23;
    }
    else if (lecture.droite)
    {
        
        return 1;
    }
    else if (lecture.centre)
    {
        
        return 2;
    }
    else if (lecture.gauche)
    {
        
        return 3;
    }
    else
    {
        return 0;
    }
    return 0;
}