/**
* \file   fonctions_capteurs.cpp
* \author Team Sac-A-Bot
* \date   22 octobre 2019
* \brief  Librairie des fonctions permettant de gérer les différents capteurs
* \version 1.0
*/

#include "fonctions_capteurs.h"

bool rfid_serie_demarre = false;

double lire_capteur_distance(int id)
{
    unsigned int valBytes = analogRead(id); //Lit le capteur et retourne une valeur entre 0 et 1023 (10 bits)
    Serial.print("valBytes: ");
    Serial.println(valBytes);
    double valVolts = ((double)valBytes * 5) / 1023; //Conversion numérique vers tension
    Serial.print("valVolts: ");
    Serial.println(valVolts);
    double distance = 27.726 * pow(valVolts, -1.2045); //Voir https://www.makerguides.com/sharp-gp2y0a21yk0f-ir-distance-sensor-arduino-tutorial/

    if (valVolts < 0.4 || valVolts > 2.5) //Si on dépasse les limites de mesure du capteur...
    {
        Serial.print("Capteur optique ");
        Serial.print(id);
        Serial.println(": Erreur de lecture.");
        return -1;
    }
    else
    {
        Serial.print("Capteur optique ");
        Serial.print(id);
        Serial.print(": ");
        Serial.print(distance);
        Serial.println("cm");
        return distance;
    }
}

/**
 * \brief Utilise les distances obtenues par la fonction de lecture du capteur de 
 *  distance et choisit la valeur médiane (donc celle la plus près du milieu) Cette
 *  fonction permet de trouver une valeur milieu plus rapidement qu'une moyenne
 * 
 * \return Retourne la valeur médiane de 5 mesures de distances
 */
double lire_median_capteur_distance(int id)

{
    double array[11]; // Création d'un array avec 5 lecture et association des valeurs
    for (int i = 0; i < 10; i++)
    {
        array[i] = lire_capteur_distance(id);
    }

    int temp = 0; // Assortissement des distances afin de les avoir en ordre croissant
    bool sorted = false;  
    while (!sorted)
    {
        sorted = true;
        for (int i = 0; i < 10; i++)
        {
            if (array[i] > array[i + 1])
            {
                sorted = false;
                temp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = temp;
            }
        }
    }
    Serial.print("distance :");
    Serial.println(array[4]);
    return array[4]; // Retour de la valeur médiane de l'array
}

/**
*\brief Permet de transformer la valeur de sortie analogique du suiveur de
*ligne en valeur digitale
*
*Retourne un entier qui exprime la valeur digitale sur 10 bits (0-1023) arrondi
*   par rapport à l'erreur du voltage (Vout n'est pas tout à fait 5V, mais 
*   donne une valeur digitale de 1017 lorsque les 3 capteurs sont activés sur
*   une valeur totale de 1024 (0-1023). La correction (1017/1024) permet
*   de corriger l'erreur du voltage.
*
*\return Retourne la valeur analogique réelle de Vout du suiveur de ligne
*/
double lire_suiveur_ligne()
{
    double valeur = analogRead(A13);
    double conversion = (valeur * 5) / 1023;
    return conversion;
}

/**
*\brief Méthode permettant de lire le capteur RFID. La broche 9 du capteur doit
*       être reliée à la broche D19 du arduino. *Doit être appelée en boucle
*       dans loop*. Inspirée de https://github.com/Thumperrr/Arduino_ID20Reader
*
*\return Le message lu par le RFID.
*/
String lire_rfid()
{
    unsigned char val_lecture = 0;
    unsigned char buffer = 0;
    unsigned char chars_lu = 0;
    unsigned char ID[6];

    if (!rfid_serie_demarre)
    {
        rfid_demarre_serie();
    }

    if (Serial1.available() > 0) //Si on a des données sur le port série
    {
        if ((val_lecture = Serial1.read()) == 0x02) //Début de trame série
        {
            chars_lu = 0;

            while (chars_lu < 12)
            {
                if (Serial1.available() != 0)
                {
                    val_lecture = Serial1.read();
                    if (val_lecture == 0x02 || val_lecture == 0x03 || val_lecture == 0x0A || val_lecture == 0x0D) //Si on sort de la plage de données
                    {
                        break;
                    }

                    val_lecture = rfid_ascii_to_hex(val_lecture);

                    //8H + 4H ---> 84H
                    if ((chars_lu % 2) == 1)
                    {
                        ID[chars_lu >> 1] = (val_lecture | (buffer << 4));
                    }
                    else
                    {
                        buffer = val_lecture;
                    }

                    chars_lu += 1;
                }
            }
        }
    }

    if (chars_lu == 12) //Si la trame est pleine
    {
        unsigned char checksum = 0;

        for (int i = 0; i < 5; ++i)
        {
            checksum ^= ID[i];
        }

        if (checksum == ID[5])
        {
            String temp;
            for (int i = 0; i < 5; ++i)
            {
                if (ID[i] < 16)
                    temp += "0" + String(ID[i], HEX);
                else
                    temp += String(ID[i], HEX);
            }
            return temp;
        }

        else
        {
            return "Erreur de checksum. Réessayer le balayage.";
        }
    }

    return "";
}

/**
*\brief Méthode qui démarre le port série relié au lecteur RFID.
*/
void rfid_demarre_serie()
{
    Serial1.begin(9600);
    rfid_serie_demarre = true;
}

/**
*\brief Méthode servant à convertir les caractères ASCII recus par le port
*       série en nombre hexadécimaux (pour le checksum);
*
*\return Le message lu par le RFID.
*/
unsigned char rfid_ascii_to_hex(unsigned char val_ascii)
{
    if (val_ascii >= '0' && val_ascii <= '9')
    {
        return val_ascii - '0';
    }
    else if (val_ascii >= 'A' && val_ascii <= 'F')
    {
        return val_ascii + 10 - 'A';
    }
    else
    {
        return 0;
    }
}

void rfid_clear_buffer()
{
    while(Serial1.available() > 0)
    {
        Serial1.read();
    }
}

Couleur lire_capteur_couleur()
{
    Couleur couleur_lue;

    return couleur_lue;
}

Lecture_Suiveur_Numerique lire_suiveur_ligne_numerique()
{
    Lecture_Suiveur_Numerique resultat;

    resultat.gauche = digitalRead(SUIVEUR_BROCHE_GAUCHE);
    resultat.droite = digitalRead(SUIVEUR_BROCHE_DROITE);
    resultat.centre = digitalRead(SUIVEUR_BROCHE_CENTRE);

    return resultat;
}