/**
 * \file   trouver_cour.cpp
 * \author Team Sac-A-Bot
 * \date   28 nvembre 2019
 * \brief  Librairie des fonctions permettant au robot de se promener de cour en cours
 * \version 1.0
 */

#include"parcour_deneigement.h"

#include "fonctions_capteurs.h"
#include <LibRobus.h>
#include <string.h>
#include "suiveur_ligne.h"
#include "fonctions_moteurs.h"


/**
*\brief retourne 1 et s'arrete devant une cours a déneiger
*
*/
int prochaine_cour()
{
    String val_RFID;
    val_RFID = lire_rfid();
    int jai_deneiger = 0;

    while(jai_deneiger == 0)
    {
        String val_RFID;
        val_RFID = lire_rfid();
        int jai_deneiger = 0;

        while(val_RFID == "")
        {
            suivre_ligne_droite();
            val_RFID = lire_rfid();

            if(est_perpendiculaire() != 1)  // arrive à un virage
            {
                reset_encoders();
                 ligne_droite(1);
                tourner_droite_luimeme(90);
            }
            
        }

        if(deneiger_ou_non(val_RFID))
        {
        while(est_perpendiculaire() != 1) // se positionner devant l'entrée
            {
            suivre_ligne();
            }
        return 1;
        jai_deneiger = 1;
        }

    }
    

    return 0;
}


/**
*\brief decide si il faut deneiger la cour
*
*return 1 si il faut deneiger
*return 0 si il ne faut pas deneiger
*
*/
bool deneiger_ou_non(String puce)
{
    if(puce == TAG_RFID_RFTR005)
    {
        return 1; //déneige
    }else if(puce == TAG_RFID_RFTR012) 
    {
        return 1; //déneige
    } 
    else if(puce == TAG_RFID_RFTR013) 
    {
        return 0;//déneige pas
    }    

    return 0;

}