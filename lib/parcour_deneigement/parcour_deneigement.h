/**
 * \file   parcour_deneigement.h
 * \author Team Sac-A-Bot
 * \date   28 novembre 2019
 * \brief  se promèV de cour en cour
 * \version 1.0
 */

#include <LibRobus.h> // Essentielle pour utiliser RobUS

int prochaine_cour();
bool deneiger_ou_non(String puce);
void sequence_deneigement();
void deneiger();

